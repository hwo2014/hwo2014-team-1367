(ns hwo2014bot.model
  (:require [incanter.core :as ii]
            [incanter.charts :as ic]
            [clojure.pprint :refer [pprint]]))

;; constants
(def throttle-factor 0.2)
(def drag-factor 0.02)
(def curve-radius 100)
(def curve-speed-factor 0.013)
(def curve-drag-factor 0.1)
(def curve-return-factor 0.006)
(comment
  (->> (run-model curve-model (constantly 6.5)) (take 55) (view-xy :t :a))
  )

(defn linear-model
  "A simple model for linear speed and acceleration, based on throttle input"
  ([] {:x 0.0 :dx 0.0 :t 0})
  ([{:keys [x dx t]} throttle]
     (let [new-dx (+ dx
                     (* throttle-factor throttle)
                     (- (* drag-factor dx)))]
       {:x  (+ x dx)
        :dx new-dx
        :t  (inc t)})))

(defn curve-model
  "A simple model for slip angle around a curve"
  ([] {:a 0.0 :da 0.0 :t 0})
  ([{:keys [a da t]} dx]
     (let [new-da (+ da
                     (* curve-speed-factor (* dx dx (/ curve-radius)))
                     (- (* curve-drag-factor da))
                     (- (* curve-return-factor a)))]
       {:a  (+ a da)
        :da new-da
        :t  (inc t)})))

(defn run-model
  "Run given model with the provided controller, producing an infinite sequence of states"
  [model-f controller-f]
  (iterate (fn [state] (model-f state (controller-f state)))
           (model-f)))

;; helper

(defn view-xy
  "View the given series of datapoints on an xy-plot"
  [x-key y-key data]
  (ii/view (ic/xy-plot (map x-key data) (map y-key data)
                       :x-label (name x-key) :y-label (name y-key))))
