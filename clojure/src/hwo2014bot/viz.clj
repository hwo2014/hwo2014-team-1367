(ns hwo2014bot.viz
  (:require [clojure.data.json :as json]
            [incanter.core :as ii]
            [incanter.charts :as ic]
            [clojure.java.io :as io]
            [clojure.pprint :refer [pprint]]))

(defn read-messages
  "Read a sequence of clojure data messages"
  [f]
  (with-open [r (java.io.PushbackReader. (io/reader f))]
    (->> #(try (read r)
               (catch Exception _ nil))
         repeatedly
         (take-while (comp not nil?))
         doall)))

(defn get-states
  "Get a sequence of states from a sequence of single-vehicle messages"
  [messages]
  (let [positions (filter (comp #{"carPositions"} :msgType) messages)]
    (remove nil?
            (map (fn [prev current]
                   (let [prev-position (-> prev :data first :piecePosition)
                         current-position (-> current :data first :piecePosition)]
                     (when (= (:pieceIndex current-position) (:pieceIndex prev-position))
                       {:tick (-> current :gameTick)
                        :speed (- (:inPieceDistance current-position)
                                  (:inPieceDistance prev-position))
                        :angle (-> current :data first :angle (Math/toRadians))})))
                 positions
                 (rest positions)))))

(defn deceleration-ratios
  "Compute (change-in-speed / speed) for each tick when decelerating"
  [samples]
  (map (fn [a b]
         (if (< b a)
            (/ (- a b) b)
            0))
       samples
       (rest samples)))

(defn accelerations
  "Compute (change-in-speed) for each tick when accelerating,
  after removing the effect of resistance (from deceleration-ratios)"
  [samples]
  (map (fn [a b]
         (if (< a b)
           (- b a (* -0.02 b))
           0))
       samples
       (rest samples)))

(defn differences
  [samples]
  (map - (rest samples) samples))

(comment
  ;; these traces can be obtained by simply (prn msg) for each message in a race
  (let [trace (->> (read-messages "log/pulse.log") get-states)
        plot (ic/xy-plot (map :tick trace) (deceleration-ratios (map :speed trace)))]
    (ic/add-lines plot (map :tick trace) (accelerations (map :speed trace)))
    (ii/view plot)
    )
  (let [trace (->> (read-messages "log/fast.log") get-states)
        plot (ic/xy-plot (map :tick trace) (map :angle trace))]
    (ic/add-lines plot (map :tick trace) (map :speed trace))
    (ii/view plot)
    )

  (let [trace (->> (read-messages "log/fast.log") get-states)
        plot (ic/xy-plot (map :tick trace) (->> trace (map :angle)))]
    (ii/view plot)
    )
  )
