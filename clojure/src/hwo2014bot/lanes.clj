(ns hwo2014bot.lanes
  "Contains utilities for processing track layout & working out which lane to take"
  (:require loom.graph
            loom.alg))

(defn- generate-edges
  "Generate all the graph edges for a given piece of track"
  [lanes piece-index piece]
  (let [direct-pieces
        (map-indexed (fn [lane-index d-center]
                       (conj [[piece-index lane-index] [(inc piece-index) lane-index]]
                             (if (:length piece)
                               (:length piece)
                               ;; positive angles are clockwise, negative anticlockwise
                               ;; negative d-center is outside (further) on a clockwise turn
                               (* (+ (:radius piece) (* -1 (Math/signum (double (:angle piece)))
                                                        d-center))
                                  (* (Math/abs (:angle piece)) (/ Math/PI 180))))))
                     lanes)]
    (if (:switch piece)
      (concat
       (mapcat (fn [[a0 b0 dist0] [a1 b1 dist1]]
                 (let [dist (/ (+ dist0 dist1) 2)]
                   [[a0 b1 dist]
                    [a1 b0 dist]]))
               direct-pieces
               (rest direct-pieces))
       direct-pieces)
      direct-pieces)))

(defn build-edge-list
  "Take a game-init message, and build an edge list representation of the graph we're using"
  [game-init]
  (let [track (-> game-init :data :race :track)
        lanes (->> track :lanes (map :distanceFromCenter))
        pieces (:pieces track)]
    (->> pieces
         (map-indexed (partial generate-edges lanes))
         (apply concat)
         (into #{}))))

(defn get-lane-switcher
  "Pass a gameInit message, returns a function (f myCarPosition) => #{:Left :Right nil}"
  [game-init]
  (let [g (apply loom.graph/weighted-graph (build-edge-list game-init))
        n-pieces (-> game-init :data :race :track :pieces count)
        n-lanes (-> game-init :data :race :track :lanes count)
        lane-paths (map (fn [lane] (loom.alg/dijkstra-path-dist g [0 lane] [n-pieces lane]))
                        (range n-lanes))]
    (fn [{piece-index :pieceIndex {lane-index :endLaneIndex} :lane}]
      (let [paths (map-indexed
                   (fn [lane [lane-path lane-cost]]
                     (let [[path path-cost] (loom.alg/dijkstra-path-dist g
                                                                         [piece-index lane-index]
                                                                         [n-pieces lane])]
                       [(concat path (rest lane-path)) (+ path-cost lane-cost)]))
                   lane-paths)
            path (first (apply min-key second paths))
            target-lane (second (nth path 2))] ;; 0: last node, 1: next node, 2: planned node
        (condp = (- target-lane lane-index)
          1  :Right
          -1 :Left
          0  nil
          (println "ERROR! Impossible lane change!"))))))
