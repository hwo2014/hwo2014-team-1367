(ns hwo2014bot.race-state)

;; helpers

(defn piece-length [piece]
  (if (:length piece)
    (:length piece)
    (* (:radius piece) (Math/abs (:angle piece)) (/ Math/PI 180))))

(defn piece-sense [piece]
  (if-let [a (:angle piece)]
    (Math/signum a)))

;; statefulness

(def state (ref {}))

(defn set-track-pieces-state!
  [game-init-msg]
  (let [tracks (get-in game-init-msg [:data :race :track :pieces])]
    (dosync
      (alter state assoc :pieces tracks))))

(defn set-my-car-state!
  [your-car-msg]
  (dosync
    (alter state assoc :my-car (:data your-car-msg))))

(defn get-car-pos
  [car-pos-msg car-name]
  (let [find-my-car-fn #(= car-name (-> % :id :name))]
    (first (filter find-my-car-fn (:data car-pos-msg)))))

(defn get-my-car-pos
  [car-pos-msg]
  (get-car-pos car-pos-msg (-> @state :my-car :name)))

(defn get-current-position
  [car-pos-msg]
  (:piecePosition (get-my-car-pos car-pos-msg)))

(defn get-current-piece
  [car-pos-msg]
  (->> (get-current-position car-pos-msg)
       :pieceIndex
       (nth (:pieces @state))))

(defn get-distance-to-bend
  [car-pos-msg]
  (let [current-piece (get-current-piece car-pos-msg)]
    (let [current-piece-index (-> (get-current-position car-pos-msg) :pieceIndex)
          current-piece-sense (piece-sense current-piece)
          two-lap-pieces (concat (-> @state :pieces) (-> @state :pieces))
          future-pieces (drop (inc current-piece-index) two-lap-pieces)
          future-pieces-before-bend (take-while #(= (piece-sense %) current-piece-sense) future-pieces)]
      (+ (- (piece-length current-piece) (-> car-pos-msg get-current-position :inPieceDistance))
         (apply + (map piece-length future-pieces-before-bend))))))

(defn set-last-position!
  [car-pos-msg]
  (let [my-car-pos (get-my-car-pos car-pos-msg)]
    (dosync
      (alter state assoc :last-position (dissoc my-car-pos :id)))))

(defn get-last-pos []
  (-> @state :last-position))

(defn get-last-position []
  (:piecePosition (get-last-pos)))

(defn get-speed
  "Gets the current speed"
  [car-pos-msg]
  (let [prev    (get-last-position)
        current (get-current-position car-pos-msg)]
    (if prev
      (+ (- (:inPieceDistance current) (:inPieceDistance prev))
         (if (not= (:pieceIndex current) (:pieceIndex prev)) ;; assume we won't tick over an entire piece!
           (-> @state :pieces (nth (:pieceIndex prev))
               piece-length)
           0))
      0)))
