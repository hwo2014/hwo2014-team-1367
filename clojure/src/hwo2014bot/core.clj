(ns hwo2014bot.core
  (:require [clojure.data.json :as json]
            [hwo2014bot.msg-utils :refer :all]
            [hwo2014bot.race-state :as state]
            [hwo2014bot.bots :as bots])
  (:gen-class))

(defn tap [x] (prn x) x)

(defmulti handle-msg :msgType)

(defmethod handle-msg "gameInit"
  [msg bot]
  (do
    (state/set-track-pieces-state! msg)
    (bots/handle-init! bot msg)
    {:msgType "ping" :data "ping"}))

(defmethod handle-msg "yourCar"
  [msg _]
  (do
    (state/set-my-car-state! msg)
    {:msgType "ping" :data "ping"}))

(defmethod handle-msg "carPositions"
  [msg bot]
  (try
    (assoc (or (bots/handle-cars! bot msg)
               {:msgType "ping" :data "ping"})
      :gameTick (:gameTick msg))
    (finally (state/set-last-position! msg))))

(defmethod handle-msg :default
  [msg _]
  {:msgType "ping" :data "ping"})

(defn log-msg [msg]
  (case (:msgType msg)
    "join" (println "Joined")
    "gameInit" (println "Race started")
    "crash" (println "Someone crashed")
    "gameEnd" (println "Race ended")
    "error" (println (str "ERROR: " (:data msg)))
    :noop))

(defn game-loop [channel bot]
  (let [msg (read-message channel)]
    ;;(prn msg)
    (log-msg msg)
    (send-message channel (handle-msg msg bot))
    (recur channel bot)))

(defn -main[& [host port botname botkey bot-type]]
  (let [channel (connect-client-channel host (Integer/parseInt port))
        bot     (bots/get-bot (keyword bot-type))]
    (send-message channel {:msgType "join" :data {:name botname :key botkey}})
    (game-loop channel bot)))
