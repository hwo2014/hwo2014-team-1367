(ns hwo2014bot.bots
  (:require [hwo2014bot.lanes :as lanes]
            [hwo2014bot.race-state :as state]))

(defprotocol Bot
  (handle-init! [_ init-msg]
    "Handle an init message")
  (handle-cars! [_ car-pos-msg]
    "Return a control for the car positions"))

(def constant-speed
  (reify Bot
    (handle-init! [_ _])
    (handle-cars! [_ m]
      {:msgType "throttle" :data 0.65})))

(def switcher-state (ref nil))
(def control-state (ref nil))
(defn switch-lane!
  "Determine if we should switch lane now to reach the optimal racing line,
  otherwise return nil"
  [car-pos-msg]
  (dosync
   (let [pos     (state/get-current-position car-pos-msg)
         control (@switcher-state pos)]
     (when (not= (-> pos :lane :startLaneIndex)
                 (-> pos :lane :endLaneIndex))
       (ref-set control-state nil))
     (when (not= control @control-state)
       (ref-set control-state control)
       (when control
         {:msgType "switchLane" :data (name control)})))))
(defn init-lane!
  [init-msg]
  (dosync
   (ref-set switcher-state (lanes/get-lane-switcher init-msg))))

(def constant-path
  (reify Bot
    (handle-init! [_ init-msg]
      (init-lane! init-msg))
    (handle-cars! [_ car-pos-msg]
      (or (switch-lane! car-pos-msg)
          {:msgType "throttle" :data 0.65}))))

(def throttle-coeff 0.2)
(def friction-coeff 0.02)
(def max-speed (/ throttle-coeff friction-coeff))

(def target-speed 7.0)
(def target-angle (Math/toRadians 55))
(def target-angle-kp 5)
(def target-angle-kd 100)

(defn control-target-angle
  "Try to maintain the target angle, using Proportional-Derivative control"
  [car-pos-msg]
  (if (:length (state/get-current-piece car-pos-msg))
    1.0
    (let [angle         (Math/toRadians (:angle (state/get-my-car-pos car-pos-msg)))
          last-angle    (Math/toRadians (:angle (state/get-last-pos)))
          error         (- target-angle (Math/abs angle))
          base-throttle (/ (state/get-speed car-pos-msg) max-speed)
          throttle
          (-> base-throttle
              (+ (* target-angle-kp error))
              (- (* target-angle-kd (- (Math/abs angle) (Math/abs last-angle))))
              (min 1) (max 0.0))]
      ;;(println "angle" angle "error" error "speed" (state/get-speed car-pos-msg) "base" base-throttle "throttle" throttle)
      throttle)))

(defn get-braking-distance
  [current-speed]
  (+ current-speed ;; a 1-frame delay, for safety
     (/ (- target-speed current-speed) (Math/log (- 1.0 friction-coeff)))))

(defn control-accelerate-coast
  "Try to attain the target speed at entry to the next corner (i.e. not including the current one)"
  [car-pos-msg]
  (let [speed       (state/get-speed car-pos-msg)
        brake-dist  (get-braking-distance speed)
        corner-dist (state/get-distance-to-bend car-pos-msg)
        is-corner   (:angle (state/get-current-piece car-pos-msg))
        throttle    (if (or is-corner (< brake-dist corner-dist))
                      1.0
                      0.0)]
    ;;(println "corner-dist" corner-dist "brake-dist" brake-dist "speed" speed "throttle" throttle)
    throttle))

(def controlled-corner
  (reify Bot
    (handle-init! [_ init-msg]
      (init-lane! init-msg))
    (handle-cars! [_ car-pos-msg]
      (or (switch-lane! car-pos-msg)
          {:msgType "throttle" :data (min (control-accelerate-coast car-pos-msg)
                                          (control-target-angle car-pos-msg))}))))


;; framework

(defn get-bot [bot-type]
  @(get (ns-publics 'hwo2014bot.bots)
        (symbol (name bot-type))
        #'constant-speed))
