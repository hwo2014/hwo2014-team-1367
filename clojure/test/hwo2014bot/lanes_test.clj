(ns hwo2014bot.lanes-test
  (:require [hwo2014bot.lanes :refer :all]
            [midje.sweet :refer :all]))

(def eg-track
  {:data {:race {:track {:pieces
                         [{:length 100 :switch true}
                          {:radius 100 :angle 270}
                          {:length 100 :switch true}
                          {:radius 100 :angle -270}]
                         :lanes
                         [{:distanceFromCenter -10, :index 0}
                          {:distanceFromCenter 10, :index 1}]}}}})

(fact "build-edge-list"
      (let [angle (* 270 (/ Math/PI 180))
            inside-curve (* 90 angle)
            outside-curve (* 110 angle)]
        (build-edge-list eg-track)
        => #{[[0 0] [1 0] 100]
             [[0 1] [1 1] 100]
             [[0 0] [1 1] 100] ; switch right
             [[0 1] [1 0] 100] ; switch left

             [[1 0] [2 0] outside-curve]
             [[1 1] [2 1] inside-curve]

             [[2 0] [3 0] 100]
             [[2 1] [3 1] 100]
             [[2 0] [3 1] 100] ; switch right
             [[2 1] [3 0] 100] ; switch left

             [[3 0] [4 0] inside-curve]
             [[3 1] [4 1] outside-curve]
             }))

(fact "get-lane-switcher"
      (let [lane-switcher (get-lane-switcher eg-track)
            switcher (fn [piece-index lane-index]
                       (lane-switcher {:pieceIndex piece-index :lane {:endLaneIndex lane-index}}))]

        (switcher 0 0) => :Right
        (switcher 0 1) => nil

        (switcher 1 0) => nil
        (switcher 1 1) => :Left

        (switcher 2 0) => nil
        (switcher 2 1) => :Left

        (switcher 3 0) => :Right
        (switcher 3 1) => nil))
