(ns hwo2014bot.race-state-test
  (:require [midje.sweet :refer :all]
            [hwo2014bot.race-state :refer :all]
            [hwo2014bot.msg-utils :refer [json->clj]]))

(defn clean-up!
  []
  (dosync
    (ref-set state {})))

(def sample-carpos-msg (json->clj (slurp "test-resources/sample-carpos.json")))

(def sample-gameinit-msg (json->clj (slurp "test-resources/sample-gameinit.json")))

(def sample-yourcar-msg (json->clj (slurp "test-resources/sample-yourcar.json")))

(facts "About `set-track-pieces-state`"
  (fact "`set-track-pieces-state` should store the pieces info into state"
    (let [_ (set-track-pieces-state! sample-gameinit-msg)]
      (count (:pieces @state)) => 3
      (first (:pieces @state)) => (just {:length (roughly 100.0)})
      (second (:pieces @state)) => (just {:length (roughly 100.0) :switch true})
      (nth (:pieces @state) 2) => (just {:radius 200 :angle (roughly 22.5)})))
  (against-background (after :facts (clean-up!))))

(facts "About `set-my-car-state`"
  (fact "`set-my-car-state` should store 'my' car information into state"
    (let [_ (set-my-car-state! sample-yourcar-msg)]
      (:my-car @state) => (just {:name "Schumacher" :color "red"})))
  (against-background (after :facts (clean-up!))))

(facts "About `set-last-position`"
  (fact "`set-last-position` should store the current position of car into state"
    (let [_ (set-track-pieces-state! sample-gameinit-msg)
          _ (set-my-car-state! sample-yourcar-msg)
          _ (set-last-position! sample-carpos-msg)]
      (:angle (:last-position @state)) => (roughly 0.0)
      (:piecePosition (:last-position @state)) => (just {:pieceIndex 0
                                                         :inPieceDistance (roughly 0.0)
                                                         :lane {:startLaneIndex 0
                                                                :endLaneIndex 0}
                                                         :lap 0})))
  (against-background (after :facts (clean-up!))))

(facts "About `get-current-piece`"
  (fact "`get-current-piece` should return the relevant piece information to the current car position"
    (let [_ (set-track-pieces-state! sample-gameinit-msg)
          _ (set-my-car-state! sample-yourcar-msg)]
      (get-current-piece sample-carpos-msg) => (just {:length (roughly 100.0)})))
  (against-background (after :facts (clean-up!))))

(facts "About `get-distance-to-bend`"
  (fact "`get-distance-to-bend` should return the distance estimated to the next bend"
    (let [_ (set-track-pieces-state! sample-gameinit-msg)
          _ (set-my-car-state! sample-yourcar-msg)]
      (get-distance-to-bend sample-carpos-msg) => (roughly 200.0)))
  (against-background (after :facts (clean-up!))))

(fact "`piece-length` should return the length of a single piece"
      (piece-length {:length 1234}) => 1234
      (piece-length {:radius 100 :angle -360}) => (roughly (* 2 Math/PI 100)))

;; TODO: get-speed needs a test
