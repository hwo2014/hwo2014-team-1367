(defproject hwo2014bot "0.1.0-SNAPSHOT"
  :description "HWO2014 Clojure Bot Template"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [aleph "0.3.2"]
                 [org.clojure/data.json "0.2.3"]
                 [org.clojure/tools.logging "0.2.6"]
                 [log4j "1.2.17"]]
  :plugins [[lein-midje "3.0.0"]]
  :main ^:skip-aot hwo2014bot.core
  :target-path "target"
  :resource-paths ["lib/loom-0.4.2.jar"]
  :profiles {:uberjar {:aot :all}
             :dev {:dependencies [[incanter "1.5.4"]
                                  [incanter/incanter-charts "1.5.4"]
                                  [midje "1.6.3"]]}})
